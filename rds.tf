resource "aws_db_instance" "default" {
  instance_class = "db.t2.micro"
  allocated_storage = 5
  storage_type = "gp2"
  engine = "mysql"
  engine_version = "5.7"
  name = "projectdb"
  username = "dbuser"
  password = "mysecurepassword"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot = "true"
}